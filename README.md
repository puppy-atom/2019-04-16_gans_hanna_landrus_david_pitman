# 2019-04-16_GANs_Hanna_Landrus_David_Pitman

ATOM is meeting on Tuesday, 16th April, 6:30pm, at Galvanize!

Our discussion this month will be led by Hanna Landrus and David Pitman.

This month we will learn about and discuss Generative Adversarial Nets (GAN). This framework pits two models against one another with one trying to approximate the training data and the other trying to determine if the examples it is shown are from the training data or from the approximation model. This technique has interesting applications from machine generated art and music to improving astronomical images. What do you want to GAN?

https://arxiv.org/pdf/1406.2661.pdf

About ATOM:
Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.

We're kindly hosted by Galvanize (https://www.galvanize.com). Thank you !